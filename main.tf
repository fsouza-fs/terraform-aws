provider "aws" {
  region = "us-east-1"
  profile = "default"
}

resource "tls_private_key" "generated-ssh-key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "my-ssh-key" {
  key_name   = "my-ssh-key"
  public_key = tls_private_key.generated-ssh-key.public_key_openssh

  provisioner "local-exec" {
    command = "echo '${tls_private_key.generated-ssh-key.private_key_pem}' > ./myKey.pem && chmod 400 ./myKey.pem"
  }
}

module "SecurityGroup" {
  source = "./modules/SecurityGroup"
}

module "EC2" {
  source = "./modules/EC2"
  secgroupid = module.SecurityGroup.security.id
}

module "Network" {
  source = "./modules/Network"
  instanceid = module.EC2.instance.id
}

resource "aws_s3_bucket" "state_bucket" {
  bucket = var.name_of_s3_bucket

  # Tells AWS to encrypt the S3 bucket at rest by default
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  # Terraform  Destroy 
  lifecycle {
    prevent_destroy = false
  }

  # Versionamento
  versioning {
    enabled = true
  }

  tags = {
    Terraform = "true"
  }
}

# DynamoDB para terraform state locking
resource "aws_dynamodb_table" "tf_lock_state" {
  name = var.dynamo_db_table_name

  billing_mode = "PAY_PER_REQUEST"

  hash_key = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name    = var.dynamo_db_table_name
    BuiltBy = "Terraform"
  }
}