variable "dynamo_db_table_name" {
  type = string
  description = "Dynamo table name"
  default = "terraform-dynamo"
}

variable "name_of_s3_bucket" {
  type = string
  description = "S3 bucket name"
  default = "terraform-bucket"
}